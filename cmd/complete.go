// Copyright © 2018 bistik <bistikred@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/bistik/task/db"
)

// completeCmd represents the complete command
var completeCmd = &cobra.Command{
	Use:   "complete",
	Short: "List all completed task",
	Run: func(cmd *cobra.Command, args []string) {
		complete, err := db.AllComplete()
		if err != nil {
			fmt.Printf("Unable to fetch completed tasks %v", err)
		}

		for i := len(complete) - 1; i >= 0; i-- {
			fmt.Printf("%v. %v\n", complete[i].Key, complete[i].Value)
		}
	},
}

func init() {
	RootCmd.AddCommand(completeCmd)
}

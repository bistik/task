// Copyright © 2018 bistik <bistikred@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/bistik/task/db"
)

// doneCmd represents the done command
var doneCmd = &cobra.Command{
	Use:   "done",
	Short: "Mark the task as done",
	Long: `Mark the task as done. For example:
task done 1`,
	Run: func(cmd *cobra.Command, args []string) {
		var ids []int

		for _, arg := range args {
			id, err := strconv.Atoi(arg)
			if err != nil {
				fmt.Println("Failed to parse the argument:", arg)
			} else {
				ids = append(ids, id)
			}
		}

		tasks, err := db.AllTasks()

		if err != nil {
			fmt.Println("Something went wrong:", err)
		}

		for _, id := range ids {
			if id <= 0 || id > len(tasks) {
				fmt.Println("Invalid task number:", id)
				continue
			}
			task := tasks[id-1]
			err := db.DeleteTask(task.Key)
			if err != nil {
				fmt.Printf("Failed to mark '%d' as completed. Error: %s\n", id, err)
			} else {
				fmt.Printf("Marked '%d' as completed\n", id)
			}

			_, err = db.CompleteTask(task.Value)
			if err != nil {
				fmt.Printf("Failed to mark '%d' as completed. Error: %s\n", id, err)
			}
		}
	},
}

func init() {
	RootCmd.AddCommand(doneCmd)
}
